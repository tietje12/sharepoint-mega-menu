import * as React from "react";
import * as ReactDOM from "react-dom";
import { createRef } from "react";
import { sp, Web } from "@pnp/sp";
import "./../AppCustomizer.module.css";

export interface INavbarProps {}

export default class Navbar extends React.Component<any, any> {
  private myRef = createRef<HTMLDivElement>();
  constructor(props: any) {
    super(props);
    this.state = {
      show: false
    };

    this.openMenu = this.openMenu.bind(this);
    this.groupByParent = this.groupByParent.bind(this);
    this.buildMenu = this.buildMenu.bind(this);
    this.buildNews = this.buildNews.bind(this);
    this.redirect = this.redirect.bind(this);
  }

  public componentDidMount() {
    this.getItems();
    this.getLatestNews();

    // Menu should close when clicking outside of it
    window.addEventListener("click", e => {
      if (!ReactDOM.findDOMNode(this).contains(e.target as Node)) {
        this.setState({
          show: false
        });
      }
    });
  }

  public replaceLink() {
    this.waitForElementToDisplay("[class^='logoWrapper']", 750);
  }

  public waitForElementToDisplay(selector, time) {
    if (
      document.querySelectorAll(selector)[0] != null &&
      typeof document.querySelectorAll(selector)[0] !== "undefined"
    ) {
      document.querySelectorAll(
        "[class^='logoWrapper']"
      )[0].attributes[1].value =
        "https://monjasa.sharepoint.com/sites/frontpage/";
      return;
    } else {
      setTimeout(() => {
        this.waitForElementToDisplay(selector, time);
      }, time);
    }
  }

  public getLatestNews() {
    let web = new Web("https://monjasa.sharepoint.com/sites/News/");

    web.lists
      .getByTitle("Site Pages")
      .items.filter(
        "ContentTypeId eq '0x0101009D1CB255DA76424F860D91F20E6C411800F808EB9B61AAEC40A0004F8C03F087890024730C8FF3150C49A00C95324CB9FD65'"
      )
      .select(
        "Created",
        "Title",
        "ContentTypeId",
        "File",
        "BannerImageUrl",
        "Description",
        "Published_x0020_Date"
      )
      .expand("File")
      .top(3)
      .orderBy("Published_x0020_Date", false)
      .get()
      .then((items: any[]) => {
        this.setState({
          news: items
        });
      });
  }

  public getItems() {
    let web = new Web("https://monjasa.sharepoint.com/sites/Frontpage/");

    web.lists
      .getByTitle("Megamenu")
      .items.get()
      .then((items: any[]) => {
        setTimeout(() => {
          this.replaceLink();
        }, 1000);

        items = items.filter(item => item.Parent);

        // Sort by parent value which is between parenthesises
        items.sort((a, b) => {
          return (
            a.Parent.substring(
              a.Parent.indexOf("(") + 1,
              a.Parent.indexOf(")")
            ) -
            b.Parent.substring(b.Parent.indexOf("(") + 1, b.Parent.indexOf(")"))
          );
        });

        this.groupByParent(items);
      });
  }

  public groupByParent(items) {
    const parents = items.reduce(
      (
        links,
        {
          Parent,
          Title,
          Url,
          Open_x0020_in_x0020_new_x0020_ta,
          Send_x0020_email,
          Email_x0020_Title,
          Email
        }
      ) => {
        if (!links[Parent]) links[Parent] = [];

        var order = Parent.substring(
          Parent.indexOf("(") + 1,
          Parent.indexOf(")")
        );

        links[Parent].push({
          Title: Title,
          Url: Url,
          Tab: Open_x0020_in_x0020_new_x0020_ta,
          SendEmail: Send_x0020_email,
          Email: Email,
          EmailTitle: Email_x0020_Title
        });
        links[Parent].order = order;
        return links;
      },
      {}
    );

    var newHashmap = [];
    Object.keys(parents).forEach(key => {
      var value = parents[key];
      var order = parents[key].order;

      key = key.replace("(" + order + ")", "");

      newHashmap[key] = value;
    });
    this.setState({
      items: newHashmap
    });
  }
  public buildNews() {
    const items = this.state.news;
    let structure = [];
    let parent = [];
    let children = [];
    items.forEach(item => {
      item.Description = item.Description ? item.Description : "";

      let link = "https://monjasa.sharepoint.com" + item.File.ServerRelativeUrl;
      children.push(
        <a href={link}>
          <div className="ms-Grid-row news-grid">
            <div className="ms-Grid-col ms-lg4">
              <div
                className="news-image"
                style={{
                  backgroundImage: "url(" + item.BannerImageUrl.Url + ")"
                }}
              ></div>
            </div>
            <div className="ms-Grid-col ms-lg8">
              <h3 className="news-title">{item.Title}</h3>
              <p className="news-desc">
                {item.Description.substring(0, 150)}...
              </p>
            </div>
          </div>
        </a>
      );
    });
    parent.push(<ul className="menu-child-list">{children}</ul>);
    return parent;
  }
  public buildMenu() {
    const items = this.state.items;
    let structure = [];

    for (let key in items) {
      let parent = [];
      let children = [];
      items[key].forEach(element => {
        let target = "_self";
        if (element.Tab) {
          target = "_blank";
        }
        if (element.SendEmail == true) {
          children.push(
            <li className="menu-child-title">
              <a href={"mailto:" + element.Email} target={target}>
                {element.EmailTitle}
              </a>
            </li>
          );
        } else {
          children.push(
            <li className="menu-child-title">
              <a href={element.Url} target={target}>
                {element.Title}
              </a>
            </li>
          );
        }
      });
      parent.push(<h2 className="menu-parent">{key}</h2>);
      parent.push(<ul className="menu-child-list">{children}</ul>);
      structure.push(
        <div className="ms-Grid-col ms-sm12 ms-md4 ms-lg4 ms-xl3 ms-xxl2">
          <div className="inner-menu-list">{parent}</div>
        </div>
      );
    }
    return structure;
  }

  public redirect() {
    window.location.href = "https://monjasa.sharepoint.com/sites/News";
  }

  public openMenu() {
    if (this.state.show) {
      this.setState({
        show: false
      });
    } else {
      this.setState({
        show: true
      });
    }
  }

  public render(): JSX.Element {
    return (
      <div className="App" id="MegaMenu" ref={this.myRef}>
        <div className="menu-trigger">
          <p
            className={"menu-title " + (this.state.show ? "close" : "open")}
            onClick={this.openMenu}
          >
            Menu
          </p>
        </div>
        {this.state.show && (
          <div className="mega-wrapper">
            <div className="ms-Grid" dir="ltr">
              <div className="ms-Grid-col ms-sm12 ms-md12 ms-lg12 ms-xl12 ms-xxl9">
                <div className="ms-Grid-row">{this.buildMenu()}</div>
              </div>
              <div className="ms-Grid-col ms-sm3 ms-md3 ms-lg3 right-block ms-u-hiddenXlDown">
                <div className="inner-campaign">
                  <div className="ms-Grid-row">
                    <div className="ms-Grid-col ms-lg12">
                      <div className="inner-menu-list">
                        <h2
                          className="menu-parent"
                          style={{ cursor: "pointer" }}
                          onClick={this.redirect}
                        >
                          Latest News
                        </h2>
                        {this.buildNews()}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
