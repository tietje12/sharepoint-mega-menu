import * as React from "react";
import * as ReactDOM from "react-dom";
import { override } from "@microsoft/decorators";
import { Log } from "@microsoft/sp-core-library";
import {
  BaseApplicationCustomizer,
  PlaceholderContent,
  PlaceholderName,
  PlaceholderProvider
} from "@microsoft/sp-application-base";

import { escape } from "@microsoft/sp-lodash-subset";

import Navbar, { INavbarProps } from "./components/Navbar";

const LOG_SOURCE: string = "COBApplicationCustomizer";

// used if you wish to pass properties to your extension..
export interface ICob12ApplicationCustomizerProperties {
  TopContent: string;
}

export default class Cob12ApplicationCustomizer extends BaseApplicationCustomizer<
  ICob12ApplicationCustomizerProperties
> {
  private _onDispose(): void {}

  private onRender(): void {
    const header: PlaceholderContent = this.context.placeholderProvider.tryCreateContent(
      PlaceholderName.Top,
      {
        onDispose: this._onDispose
      }
    );
    if (!header) {
      Log.error(LOG_SOURCE, new Error("Could not find placeholder PageHeader"));
      return;
    }
    if (header.domElement.parentElement.getElementsByClassName("App").length) {
      return;
    }
    const elem: React.ReactElement<INavbarProps> = React.createElement(Navbar);
    ReactDOM.render(elem, header.domElement);
  }

  @override
  public onInit(): Promise<void> {
    this.onRender();
    return Promise.resolve<void>();
  }
}
